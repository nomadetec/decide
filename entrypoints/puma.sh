#!/bin/bash
set -e;

if ! [ -f /opt/.db-migrated ]; then
  bundle exec rails db:migrate
  touch /opt/.db-migrated
fi

if [ -n "$DATABASE_SEEDING" ]; then
  if ! [ -f /opt/.db-seeded ]; then
    bundle exec rails db:seed
    touch /opt/.db-seeded
  fi
else
  if [ -n "$SYSTEM_ADMIN_EMAIL" ] && [ -n "$SYSTEM_ADMIN_PASSWORD" ] && ! [ -f /opt/.db-system-admin-created ]; then
    echo "Creating system admin..."
    bundle exec rails runner "Decidim::System::Admin.create(email: '$SYSTEM_ADMIN_EMAIL', password: '$SYSTEM_ADMIN_PASSWORD', password_confirmation: '$SYSTEM_ADMIN_PASSWORD')"
    touch /opt/.db-system-admin-created
  fi
fi

[ -f tmp/pids/server.pid ] && rm -f tmp/pids/server.pid

bundle exec puma -p 3000 -C config/puma.rb
