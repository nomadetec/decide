#!/bin/bash
set -e;

touch log/cron.log && cron && whenever --update-crontab && crontab -l && tail -f log/cron.log
