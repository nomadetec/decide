#!/bin/bash
set -e;

psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
  -- Decidim
  CREATE ROLE ${DATABASE_USERNAME} LOGIN ENCRYPTED PASSWORD '${DATABASE_PASSWORD}' NOINHERIT VALID UNTIL 'infinity';
  CREATE DATABASE ${DATABASE_NAME} WITH ENCODING='UTF8' OWNER=${DATABASE_USERNAME};
  GRANT ALL PRIVILEGES ON DATABASE ${DATABASE_NAME} TO ${DATABASE_USERNAME};
  -- Etherpad
  CREATE ROLE ${ETHERPAD_DB_USER} LOGIN ENCRYPTED PASSWORD '${ETHERPAD_DB_PASS}' NOINHERIT VALID UNTIL 'infinity';
  CREATE DATABASE ${ETHERPAD_DB_NAME} WITH ENCODING='UTF8' OWNER=${ETHERPAD_DB_USER};
  GRANT ALL PRIVILEGES ON DATABASE ${ETHERPAD_DB_NAME} TO ${ETHERPAD_DB_USER};
EOSQL

psql -U postgres -d ${DATABASE_NAME} -c "GRANT ALL ON SCHEMA public TO ${DATABASE_USERNAME};"

psql -U postgres -d ${ETHERPAD_DB_NAME} -c "GRANT ALL ON SCHEMA public TO ${ETHERPAD_DB_USER};"
