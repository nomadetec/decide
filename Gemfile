# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby RUBY_VERSION

DECIDIM_VERSION = '0.27.9'

# CALENDAR_REPO = { path: '../decidim-module-calendar' }.freeze
CALENDAR_REPO = { github: 'luizsanches/decidim-module-calendar' }.freeze

#SLIDER_REPO = { path: '../decidim-module-slider' }.freeze
SLIDER_REPO = { github: 'luizsanches/decidim-module-slider' }.freeze

gem 'decidim', DECIDIM_VERSION
gem 'decidim-conferences', DECIDIM_VERSION
gem 'decidim-consultations', DECIDIM_VERSION
gem 'decidim-initiatives', DECIDIM_VERSION
gem 'decidim-templates', DECIDIM_VERSION

gem 'decidim-calendar', CALENDAR_REPO
gem 'decidim-slider', SLIDER_REPO
gem 'decidim-apiauth', github: 'mainio/decidim-module-apiauth', branch: 'release/0.27-stable'
gem 'decidim-term_customizer', github: 'mainio/decidim-module-term_customizer',
                               branch: 'release/0.27-stable'#, require: false
gem 'decidim-decidim_awesome', '>= 0.9'

gem 'dotenv-rails', require: 'dotenv/load'

gem 'bootsnap', '1.18.3', require: false

gem 'puma', '5.6.8'
gem 'uglifier', '~> 4.1'

gem 'devise-i18n'

gem 'sidekiq', '7.2.2'

gem 'whenever', require: false

gem 'dalli', '3.2.8' # For memcached

gem 'wicked_pdf', '2.7.0'

group :development, :test do
  gem 'byebug', '11.1.3'
  gem 'decidim-dev', DECIDIM_VERSION
  gem 'faker', '2.19.0'
end

group :development do
  gem 'letter_opener_web', '~> 1.3'
  gem 'listen', '~> 3.1'
  gem 'spring', '~> 2.0'
  gem 'spring-watcher-listen', '~> 2.0'
  gem 'web-console', '~> 3.5'
end

group :production do
  gem 'aws-sdk-s3', require: false
end

group :development, :production do
  gem 'rails_performance', '1.2.1'
end
