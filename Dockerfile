FROM ruby:3.0.6-slim-bullseye as build

ARG RAILS_ENV=production

WORKDIR /decide

COPY Gemfile Gemfile.lock .

RUN apt update \
 && apt install -y git libpq-dev make g++ libicu-dev \
 && bundle install

COPY . .

RUN apt update \
 && apt install -y curl gnupg2 \
 && mkdir /etc/apt/keyrings \
 && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
 && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_18.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
 && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | tee /etc/apt/trusted.gpg.d/yarnpkg.asc \
 && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
 && apt update \
 && apt install -y nodejs yarn \
 && yarn install \
 && sed -i -e "s|branch: 'release\/0.27-stable'#, require: false|branch: 'release\/0.27-stable', require: false|" Gemfile \
 && bundle exec rails assets:precompile \
 && sed -i -e "s|branch: 'release\/0.27-stable', require: false|branch: 'release\/0.27-stable'#, require: false|" Gemfile \
 && bundle exec rails tmp:clear \
 && bundle exec rails log:clear \
 && yarn cache clean \
 && rm -rf node_modules

FROM ruby:3.0.6-slim-bullseye

LABEL org.opencontainers.image.authors="sanches@nomade.tec.br"

ENV TZ='America/Belem'

WORKDIR /decide

COPY --from=build /usr/local/bundle /usr/local/bundle
COPY --from=build /decide /decide

RUN apt update \
 && apt install -y git nodejs libpq-dev imagemagick \
 && apt clean autoclean \
 && bundle install
