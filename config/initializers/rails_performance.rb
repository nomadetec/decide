RailsPerformance.setup do |config|
  perform = ENV['REDIS_PERFORMANCE_URL'].present?

  config.enabled = perform

  config.redis =  if perform
                    Redis::Namespace.new(
                      "#{Rails.env}-rails-performance",
                      redis: Redis.new(url: ENV['REDIS_PERFORMANCE_URL'])
                    )
                  else
                    nil
                  end

  config.duration = 5.hours

  config.debug = false # currently not used

  # default path where to mount gem
  config.mount_at = '/performance'

  if Rails.env.production?
    # protect your Performance Dashboard with HTTP BASIC password
    config.http_basic_authentication_enabled = true
    config.http_basic_authentication_user_name = ENV['ADMIN_USERNAME']
    config.http_basic_authentication_password = ENV['ADMIN_PASSWORD']
  end

  # if you need an additional rules to check user permissions
  config.verify_access_proc = proc { |controller| true }
  # for example when you have `current_user`
  # config.verify_access_proc = proc { |controller| controller.current_user && controller.current_user.admin? }

  # You can ignore endpoints with Rails standard notation controller#action
  # config.ignored_endpoints = ['HomeController#contact']

  # config home button link
  config.home_link = '/'
end if defined?(RailsPerformance)
