#!/bin/bash

command -v docker compose &> /dev/null
[ $? -ne 0 ] && echo "Docker compose is not installed" && exit 1

COMMAND=$1
ARGUMENTS=$@
ETHER_KEY=".etherpad-apikey"
COMPOSES="-f docker-compose.yml -f docker-compose-dev.yml -f docker-compose-etherpad.yml"

if [ "$RAILS_ENV" == "production" ]; then
  COMPOSES=
fi

case "$COMMAND" in
  up)
    [ ! -f "$ETHER_KEY" ] && echo $RANDOM | sha256sum | cut -d' ' -f1 > $ETHER_KEY

    docker compose $COMPOSES $ARGUMENTS
    ;;
  ps)
    watch docker compose $COMPOSES ps
    ;;
  build | stop | start | down | logs)
    docker compose $COMPOSES $ARGUMENTS
    ;;
  open)
    command -v xdg-open &> /dev/null
    [ $? -ne 0 ] && echo "xdg-open is not installed" && exit 1

    xdg-open http://localhost:3000
    xdg-open http://localhost:1080
    xdg-open http://localhost:9002
    xdg-open http://localhost:9003
    ;;
  bash)
    docker compose exec -it cron bash
    ;;
  console)
    docker compose exec -it cron bundle exec rails console
    ;;
  tasks)
    docker compose exec -it cron bash tasks.sh
    ;;
  tail)
    tail -f log/development.log
    ;;
  *)
    echo "Allowed commands: build, up, ps, stop, start, down, logs, open, console, tasks, tail"
    exit 1
    ;;
esac
