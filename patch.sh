#!/bin/bash

RUBY_VERSION=$(cat .ruby-version)
RUBY_SERIE=$(echo $RUBY_VERSION | cut -d. -f1-2)
DECIDIM_VERSION=$(grep "DECIDIM_VERSION =" Gemfile | cut -d"'" -f2)
RBENV_PATH="/home/$USER/.rbenv/versions/$RUBY_VERSION/lib/ruby/gems/$RUBY_SERIE.0/gems"

# https://github.com/decidim/decidim/blob/v0.27.9/decidim-meetings/app/events/decidim/meetings/meeting_registration_notification_event.rb

MEETING_REGISTRATION_NOTIFICATION="$RBENV_PATH/decidim-meetings-$DECIDIM_VERSION/app/events/decidim/meetings/meeting_registration_notification_event.rb"
CODE_LINE="def email_intro"

RESULT=$(grep "$CODE_LINE" "$MEETING_REGISTRATION_NOTIFICATION")

if [ -z "$RESULT" ]; then
  sed -i -e "/include Decidim::Events::NotificationEvent/ a \
  \ \n \
  \   def email_intro\n        I18n.t(\"email_intro\", **i18n_options).html_safe\n      end" $MEETING_REGISTRATION_NOTIFICATION
fi

# https://github.com/decidim/decidim/blob/v0.27.9/decidim-conferences/app/events/decidim/conferences/conference_registration_notification_event.rb

CONFERENCE_REGISTRATION_NOTIFICATION="$RBENV_PATH/decidim-conferences-$DECIDIM_VERSION/app/events/decidim/conferences/conference_registration_notification_event.rb"
CODE_LINE="def email_intro"

RESULT=$(grep "$CODE_LINE" "$CONFERENCE_REGISTRATION_NOTIFICATION")

if [ -z "$RESULT" ]; then
  sed -i -e "/include Decidim::SanitizeHelper/ a \
  \ \n \
  \   def email_intro\n        I18n.t(\"email_intro\", **i18n_options).html_safe\n      end" $CONFERENCE_REGISTRATION_NOTIFICATION
fi
